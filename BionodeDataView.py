from PyQt4 import QtGui, QtCore
import pyqtgraph as pg
from BionodeDataView_UI import Ui_Form
#from BionodeDataView_ConfigFileManager_UI import Ui_Form as Ui_Form_ConfigFileManager

import socket
import threading
import datetime
import configparser
import os.path
import BaseStation
import select
import time

UpdatePlotPeriod = 50
#Remove this
BytesPerBSPacket = BaseStation.BytesPerBSPacket
CRCPoly = BaseStation.CRCPoly

#SampleFrequencyStringMap = {'12.5 kHz': 12500, '6.25 kHz': 6250, '5 kHz': 5000, '2.5 kHz': 2500, '1.25 kHz': 1250, '500 Hz': 500, '250 Hz': 250}

class ControlMainWindow(QtGui.QMainWindow, Ui_Form):
    def __init__(self, parent=None):
        super(ControlMainWindow, self).__init__(parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)

        self.ui.Version_l.setText('Version 16.06.06')

        self.numberOfChannelsDisplayed = 2
        self.SampleFrequencyStringMap = {'12.5 kHz': 12500, '6.25 kHz': 6250, '5 kHz': 5000, '2.5 kHz': 2500, '1.25 kHz': 1250, '500 Hz': 500, '250 Hz': 250}

        self.SampleFrequencyStrings_2ch = ['500 Hz', '2.5 kHz', '5 kHz', '12.5 kHz']
        self.SampleFrequencyStrings_4ch = ['250 Hz', '1.25 kHz', '2.5 kHz', '6.25 kHz']

        self.ADCCodes = {'Rec 1': 1, 'Rec 2': 2, 'Opt 1': 3, 'Opt 2': 4, 'Rs_H': 5, 'Rs_L': 6}

        self.PAPowerRegisters = {'0.05': 0x00, '0.06': 0x01, '0.08': 0x02, '0.10': 0x03, '0.13': 0x04, '0.16': 0x05, '0.20': 0x06, '0.25': 0x07, '0.30': 0x08, '0.38': 0x09, '0.47': 0x0A, '0.59': 0x0B, '0.74': 0x0C, '0.93': 0x0D, '1.17': 0x0E, '1.45': 0x0F, '1.78': 0x10, '2.14': 0x11, '2.57': 0x12, '2.95': 0x13, '3.24': 0x14, '3.47': 0x15, '3.72': 0x16, '3.80': 0x17, '3.89': 0x18, '3.98': 0x19, '4.07': 0x1A, '4.17': 0x1B}

        self.closing = False
        self.stoppedSocket = True
        self.socketsOpened = False
        
        self.BionodeConfigSettingsPath = 'BionodeConfigSettings.ini'
        self.BaseStationConfigSettingsPath = 'BaseStationConfigSettings.ini'
        self.VoltsPerBit = 1.2/256
        self.SampleRate = self.getSampleRateFromComboBox()/2
        self.WindowSize = 5000
        self.dataToDisplay = []
        self.displayData = [-2 for i in range(self.WindowSize*2)]

        self.prevTimeStamp = -1
        self.recording = False
        self.recordingFileSize = 0

        self.newThermalDataReady = False
        self.thermalData = -1

        self.ch1gain = 1000
        self.ch2gain = 1000
        self.ch3gain = 1000
        self.ch4gain = 1000

        self.displayPaused = False

        self.SetUpDisplays()
        self.updatePlotVisibility(2)

        self.ui.ADC_3_l.setVisible(False)
        self.ui.ADC_4_l.setVisible(False)
        self.ui.ADC3Input_cb.setVisible(False)
        self.ui.ADC4Input_cb.setVisible(False)

        #hide the impedance test buttons and labels
        self.ui.StartImpedanceTest_b.setVisible(False)
        self.ui.StopImpedanceTest_b.setVisible(False)
        self.ui.ImpedanceTestChannel_l.setVisible(False)
        self.ui.ImpedanceTestCh_cb.setVisible(False)

        #Connect UI Actions
        self.connectActions()

        #Set defauld textbox values
        self.ui.PulseAmplitude_te.setText('300')
        self.ui.PulseWidth_te.setText('1000')
        self.ui.PulsePeriod_te.setText('5000')
        self.ui.NumStimCycles_te.setText('100')
        self.ui.PositiveCalibration_te.setText('0')
        self.ui.NegativeCalibration_te.setText('0')
        self.ui.CathodicCalibration_te.setText('0')
        self.ui.AnodicCalibration_te.setText('0')
        self.ui.ZeroCalibration_te.setText('0')
        self.ui.Pot1Value_te.setText('128')
        self.ui.Pot2Value_te.setText('128')
        self.ADCChannelRatio = self.ui.ADCSampleRatio_cb.currentText()
        #self.ui.ChargeFrequency_te.setText('300')
        self.ui.ChargeFrequency_dsb.setValue(345.00)

        #If bionode config file exists, read it in and populate the Bionode combo box
        if (os.path.isfile(self.BionodeConfigSettingsPath)):
            self.bionodeConfig = configparser.ConfigParser()
            self.bionodeConfig.read(self.BionodeConfigSettingsPath)
            for bionode in self.bionodeConfig.sections():
                self.ui.Bionode_cb.addItem(self.ConfigSectionMap(self.bionodeConfig,bionode)['name'])

        if (os.path.isfile(self.BaseStationConfigSettingsPath)):
            self.baseStationConfig = configparser.ConfigParser()
            self.baseStationConfig.read(self.BaseStationConfigSettingsPath)
            for baseStation in self.baseStationConfig.sections():
                self.ui.BaseStationAddress_cb.addItem(self.ConfigSectionMap(self.baseStationConfig,baseStation)['address'])

    def closeEvent(self, event):
        print('closing')

        try:
            self.closing = True
            startTime = time.time()
            if (self.socketsOpened):
                while not self.stoppedSocket:
                    if ((time.time() - startTime) > 5):
                        print('Timeout!')
                        break
                self.BaseStationController.CloseSockets()
            else:
                print('No Sockets open!')
        except:
            print('errors on closing...')
        event.accept()

    def ConfigSectionMap(self,config,section):
        dict1 = {}
        options = config.options(section)
        for option in options:
            try:
                dict1[option] = config.get(section, option)
            except:
                dict1[option] = None
        return dict1

    def SetUpDisplays(self):
        self.plotTimer = QtCore.QTimer()
        self.plotTimer.timeout.connect(self.updatePlot)   

        #Set up the first plot
        self.pw1 = self.ui.graphicsView_1.getPlotItem()
        self.p1 = self.pw1.plot()
        self.p1.setPen((200,200,0))
        self.pw1.setXRange(0, self.WindowSize/(self.SampleRate))
        self.pw1.setYRange(-self.VoltsPerBit*128/1000, self.VoltsPerBit*128/1000)
        self.pw1.setTitle(title=self.ui.ADC1Input_cb.currentText())
        self.pw1.setLabel('bottom',text='Time',units='s')
        self.pw1.showLabel('bottom',show=True)
        self.pw1.setLabel('left',text='EMG',units='V')
        self.pw1.showLabel('left',show=True)
        self.pw1.showGrid(x=True,y=True)

        #Set up the second plot
        self.pw2 = self.ui.graphicsView_2.getPlotItem()
        self.p2 = self.pw2.plot()
        self.p2.setPen((50,200,0))
        self.pw2.setXRange(0, self.WindowSize/(self.SampleRate))
        self.pw2.setYRange(-self.VoltsPerBit*128/1000, self.VoltsPerBit*128/1000)
        self.pw2.setTitle(title=self.ui.ADC2Input_cb.currentText())
        self.pw2.setLabel('bottom',text='Time',units='s')
        self.pw2.showLabel('bottom',show=True)
        self.pw2.setLabel('left',text='EMG',units='V')
        self.pw2.showLabel('left',show=True)
        self.pw2.showGrid(x=True,y=True)

        #Set up the third plot
        self.pw3 = self.ui.graphicsView_3.getPlotItem()
        self.p3 = self.pw3.plot()
        self.p3.setPen((100,200,200))
        self.pw3.setXRange(0, self.WindowSize/(self.SampleRate))
        self.pw3.setYRange(-self.VoltsPerBit*128/1000, self.VoltsPerBit*128/1000)
        self.pw3.setTitle(title=self.ui.ADC3Input_cb.currentText())
        self.pw3.setLabel('bottom',text='Time',units='s')
        self.pw3.showLabel('bottom',show=True)
        self.pw3.setLabel('left',text='EMG',units='V')
        self.pw3.showLabel('left',show=True)
        self.pw3.showGrid(x=True,y=True)

        #Set up the fourth plot
        self.pw4 = self.ui.graphicsView_4.getPlotItem()
        self.p4 = self.pw4.plot()
        self.p4.setPen((200,0,200))
        self.pw4.setXRange(0, self.WindowSize/(self.SampleRate))
        self.pw4.setYRange(-self.VoltsPerBit*128/1000, self.VoltsPerBit*128/1000)
        self.pw4.setTitle(title=self.ui.ADC4Input_cb.currentText())
        self.pw4.setLabel('bottom',text='Time',units='s')
        self.pw4.showLabel('bottom',show=True)
        self.pw4.setLabel('left',text='EMG',units='V')
        self.pw4.showLabel('left',show=True)
        self.pw4.showGrid(x=True,y=True)

    def connectActions(self):
        self.ui.Record_b.clicked.connect(self.myRecordButtonClicked)
        self.ui.Update_b.clicked.connect(self.myUpdateButtonClicked)
        self.ui.StartStim_b.clicked.connect(self.myStartStimButtonClicked)
        self.ui.StopStim_b.clicked.connect(self.myStopStimButtonClicked)
        self.ui.Connect_b.clicked.connect(self.myConnectButtonClicked)
        self.ui.UpdateBaseStation_b.clicked.connect(self.myUpdateBaseStationButtonClicked)
        self.ui.PowerAmplifierOn_cb.stateChanged.connect(self.myPowerAmplifierOnCheckBoxChecked)
        self.ui.Shutdown_b.clicked.connect(self.myShutDownButtonClicked)
        self.ui.PairBaseStation_b.clicked.connect(self.myPairBaseStationButtonClicked)
        self.ui.StartImpedanceTest_b.clicked.connect(self.myStartImpedanceTestButtonClicked)
        self.ui.StopImpedanceTest_b.clicked.connect(self.myStopImpedanceTestButtonClicked)
        self.ui.AddIP_b.clicked.connect(self.myIPAddButtonClicked)
        self.ui.PauseDisplay_b.clicked.connect(self.myPauseDisplayButtenClicked)
        self.ui.Enable4Channels_cb.stateChanged.connect(self.myEnable4ChannelsCheckBoxChecked)
        self.ui.AutoFrequencyOn_cb.stateChanged.connect(self.myAutoFrequencyCheckBoxChecked)

    def updatePlotVisibility(self,numberOfPlots):
        if (numberOfPlots == 2):
            self.ui.graphicsView_1.setGeometry(QtCore.QRect(10, 280, 811, 201))
            self.ui.graphicsView_2.setGeometry(QtCore.QRect(10, 490, 811, 201))
            self.ui.graphicsView_3.setVisible(False)
            self.ui.graphicsView_4.setVisible(False)
        else:
            self.ui.graphicsView_1.setGeometry(QtCore.QRect(10, 280, 401, 201))
            self.ui.graphicsView_2.setGeometry(QtCore.QRect(10, 490, 401, 201))
            self.ui.graphicsView_3.setVisible(True)
            self.ui.graphicsView_4.setVisible(True)

    def myAutoFrequencyCheckBoxChecked(self,state):
        if (state == 2):
            self.ui.ChargeFrequency_dsb.setEnabled(False)
            self.ui.label_13.setEnabled(False)
            self.ui.label_14.setEnabled(False)
        else:
            self.ui.ChargeFrequency_dsb.setEnabled(True)
            self.ui.label_13.setEnabled(True)
            self.ui.label_14.setEnabled(True)

    def myEnable4ChannelsCheckBoxChecked(self,state):

        #Consider changing plot visibility only after the update button is pressed
        if (state == 2):
            self.ui.ADC_3_l.setVisible(True)
            self.ui.ADC_4_l.setVisible(True)
            self.ui.ADC3Input_cb.setVisible(True)
            self.ui.ADC4Input_cb.setVisible(True)
            self.ui.ADCSampleRatio_cb.setEnabled(False)
            self.ui.ADCSampleRatio_l.setEnabled(False)
            self.ui.ADCSampleRatio_cb.setCurrentIndex(0)

            for index in range(self.ui.SampleFrequency_cb.count()):
                self.ui.SampleFrequency_cb.setItemText(index, self.SampleFrequencyStrings_4ch[index])

        else:
            self.ui.ADC_3_l.setVisible(False)
            self.ui.ADC_4_l.setVisible(False)
            self.ui.ADC3Input_cb.setVisible(False)
            self.ui.ADC4Input_cb.setVisible(False)
            self.ui.ADCSampleRatio_cb.setEnabled(True)
            self.ui.ADCSampleRatio_l.setEnabled(True)
            self.ui.ADCSampleRatio_cb.setCurrentIndex(0)

            for index in range(self.ui.SampleFrequency_cb.count()):
                self.ui.SampleFrequency_cb.setItemText(index, self.SampleFrequencyStrings_2ch[index])

    def myPauseDisplayButtenClicked(self):
        if (self.displayPaused == False):
            self.displayPaused = True
            self.ui.PauseDisplay_b.setText('Resume Display')
        else:
            self.displayPaused = False
            self.ui.PauseDisplay_b.setText('Pause Display')
        # self._filter = Filter()
        # self.ui.ChargeFrequency_te.installEventFilter(self._filter)

    def myIPAddButtonClicked(self):

        text, ok = QtGui.QInputDialog.getText(self, "Add a Base Station", "Enter Base Station IP Address:", QtGui.QLineEdit.Normal,'192.168.1.x')

        if ok and text != '':
            self.ui.BaseStationAddress_cb.addItem(text)

    def myPairBaseStationButtonClicked(self):
        while (self.ui.BaseStationAddress_cb.count() > 0):
            self.ui.BaseStationAddress_cb.removeItem(self.ui.BaseStationAddress_cb.count()-1)
        addresses = []
        print("Listening for incoming messages...")
        UDP_PORT = 9000
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
        sock.setblocking(0)
        sock.bind(('', UDP_PORT))

        ready = select.select([sock], [], [], 2)

        baseStationFound = False
        baseStationAddress = '<emulation>'

        startTime = time.time()

        while (time.time() - startTime) < 5:
            if (ready[0]):
                newData, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
                ready = select.select([sock], [], [], 2)
                #uncomment for it to work!
                if len(newData) > 0:
                    i = 0
                    validPackets = []
                    for p in range(int(len(newData)/BytesPerBSPacket)):
                        rawData = newData[i:i+BytesPerBSPacket]
                        i += BytesPerBSPacket

                        packet = BaseStation.GetPacket(rawData, CRCPoly)

                        if (packet.valid):
                            validPackets.append(packet)
                        else:
                            print('Invalid Data Packet!')

                    #Route all valid packets to their correct positions
                    for p in validPackets:
                        if p.PacketType == BaseStation.DataPacketType.IDResponse:
                            baseStationFound = True
                            baseStationAddress = addr[0]
                            if not(addr[0] in addresses):
                                addresses.append(addr[0])

        for a in addresses:
            self.ui.BaseStationAddress_cb.addItem(str(a))

    def myShutDownButtonClicked(self):
        self.BaseStationController.SendShutDownPacketToBaseStation()

    def myPowerAmplifierOnCheckBoxChecked(self,state):
        if (state == 2):
            reply = QtGui.QMessageBox.warning(self, 'Message', 'Are you sure you want to turn on the Power Amplifier?', QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)

            if reply == QtGui.QMessageBox.Yes:
                pass
            else:
                self.ui.PowerAmplifierOn_cb.setCheckState(0)

        else:
            pass

    def myUpdateBaseStationButtonClicked(self):
        self.UpdateBaseStationRegistersFromUI(False)

    def myConnectButtonClicked(self):
         #Create the Base Station Controller
        self.UDP_TX_Port = 9001
        self.UDP_RX_Port = 9000
        self.UDP_RX_IP = ''
        self.UDP_TX_IP = self.ui.BaseStationAddress_cb.currentText()
        if (self.UDP_TX_IP == '<emulation>'):
            self.UDP_TX_IP = self.UDP_RX_IP

        self.BaseStationController = BaseStation.BaseStationController(BaseStation.BytesPerBSPacket, BaseStation.CRCPoly, self.UDP_TX_Port, self.UDP_RX_Port, self.UDP_TX_IP, self.UDP_RX_IP)

        self.socketsOpened = True

        #Update BaseStation Registers. Do not reset the timestamp counter
        self.UpdateBaseStationRegistersFromUI(False)

        #Kick off datalistener thread
        self.thread = threading.Thread(target=self.data_listener)
        self.thread.daemon = True
        self.thread.start()

        #Start plotTimer
        self.plotTimer.start(UpdatePlotPeriod)

        self.ui.Connections_gb.setEnabled(False)
        self.ui.BaseStationParameters_gb.setEnabled(True)
        self.ui.BionodeRecordingParameters_gb.setEnabled(True)
        self.ui.BionodeStimulusParameters_gb.setEnabled(True)
        
        self.ui.PositiveCalibration_te.setEnabled(True)
        self.ui.NegativeCalibration_te.setEnabled(True)
        self.ui.AnodicCalibration_te.setEnabled(True)
        self.ui.CathodicCalibration_te.setEnabled(True)

        self.ui.Pot1Value_te.setEnabled(True)
        self.ui.Pot2Value_te.setEnabled(True)
        self.ui.ReadPressure_cb.setEnabled(True)
        self.ui.Enable4Channels_cb.setEnabled(True)

        self.ui.PAPower_cb.setEnabled(True)
        self.ui.ChargeFrequency_dsb.setEnabled(True)

        self.UpdateBionodeRegistersFromUI(False, False, False, False, True)

    def myStopStimButtonClicked(self):
        self.UpdateBionodeRegistersFromUI(False,True,False,False, False)
        self.ui.StartStim_b.setEnabled(True)
        self.ui.StopStim_b.setEnabled(False)

    def myStartStimButtonClicked(self):
        self.UpdateBionodeRegistersFromUI(True,False, False, False, False)
        self.ui.StartStim_b.setEnabled(False)
        self.ui.StopStim_b.setEnabled(True)

    def myUpdateButtonClicked(self):
        self.UpdateBionodeRegistersFromUI(False, False, False, False, True)

    def myStartImpedanceTestButtonClicked(self):
        self.UpdateBionodeRegistersFromUI(False, False, True, False, False)

    def myStopImpedanceTestButtonClicked(self):
        self.UpdateBionodeRegistersFromUI(False,False, False, True, False)

    def getSampleRateFromComboBox(self):
        totalSampleRateString = self.ui.SampleFrequency_cb.currentText()
        totalSampleRate = self.SampleFrequencyStringMap[self.ui.SampleFrequency_cb.currentText()]*self.numberOfChannelsDisplayed
        return totalSampleRate

    def UpdateBaseStationRegistersFromUI(self, TimeStampReset):
        #Get the current com frequency setting for the bionode

        commFrequency = int(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['commfrequency'])
        PAPowerRegister = self.PAPowerRegisters[self.ui.PAPower_cb.currentText()]

        AutoChargeFrequency = self.ui.AutoFrequencyOn_cb.checkState() == 2
        ChargeFrequency = self.ui.ChargeFrequency_dsb.value()

        if (ChargeFrequency < 330):
            return
        if (ChargeFrequency > 350):
            return
        if(self.ui.PowerAmplifierOn_cb.checkState() == 0):
            PAEnable = False
        else:
            PAEnable = True

        #Make sure the charge frequency is not too low or too high.
        #Add a pop-up error message or something to alert the user that it's not working.


        self.BaseStationController.UpdateBSRegisters(commFrequency, PAPowerRegister, ChargeFrequency, AutoChargeFrequency, PAEnable, TimeStampReset)

    def UpdateBionodeRegistersFromUI(self, StartStim, StopStim, StartImpedanceTest, StopImpedanceTest,StartThermalTest):
        amplitude = float(self.ui.PulseAmplitude_te.toPlainText())/1000000
        pulseRepeatTime = float(self.ui.PulsePeriod_te.toPlainText())/1000000
        pulseWidth = float(self.ui.PulseWidth_te.toPlainText())/1000000
        numStimCycles = int(self.ui.NumStimCycles_te.toPlainText())
        if (self.ui.ContinuousStimulaiton_cb.isChecked()):
            numStimCycles = 0xFFFF

        positiveCalibration = int(self.ui.PositiveCalibration_te.toPlainText())
        negativeCalibration = int(self.ui.NegativeCalibration_te.toPlainText())
        cathodicCalibration = int(self.ui.CathodicCalibration_te.toPlainText())
        anodicCalibration = int(self.ui.AnodicCalibration_te.toPlainText())
        zeroCalibration = int(self.ui.ZeroCalibration_te.toPlainText())

        totalSampleRate = self.getSampleRateFromComboBox()

        ADCResolutionString = self.ui.ADCResolution_cb.currentText()

        ADCResolution = 0
        if (ADCResolutionString == '8 Bit'):
            ADCResolution = 8
        elif(ADCResolutionString == '10 Bit'):
            ADCResolution = 10

        StartImpedanceTest1 = False
        StopImpedanceTest1 = False
        StartImpedanceTest2 = False
        StopImpedanceTest2 = False

        impedanceTestChString = self.ui.ImpedanceTestCh_cb.currentText()

        if (StartImpedanceTest):
            if (impedanceTestChString == 'Rec 1'):
                StartImpedanceTest1 = True
            else:
                StartImpedanceTest2 = True
        elif (StopImpedanceTest):
            if (impedanceTestChString == 'Rec 1'):
                StopImpedanceTest1 = True
            else:
                StopImpedanceTest2 = True

        self.ADCChannelRatio = self.ui.ADCSampleRatio_cb.currentText()

        ADC1InputCode = self.ADCCodes[self.ui.ADC1Input_cb.currentText()]
        ADC2InputCode = self.ADCCodes[self.ui.ADC2Input_cb.currentText()]

        if (self.numberOfChannelsDisplayed == 4):

            ADC3InputCode = self.ADCCodes[self.ui.ADC3Input_cb.currentText()]
            ADC4InputCode = self.ADCCodes[self.ui.ADC4Input_cb.currentText()]

        else:
            ADC3InputCode = ADC1InputCode
            ADC4InputCode = ADC2InputCode

        PotCH1V1PressureData = int(self.ui.Pot1Value_te.toPlainText())
        PotCH2V1PressureData = int(self.ui.Pot2Value_te.toPlainText())

        PotCH1V2PressureData = 0
        PotCH2V2PressureData = 0

        if (self.ui.ReadPressure_cb.isChecked()):
            PressureReadingConfig = 0x1
        else:
            PressureReadingConfig = 0x0

        version = self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['version']

        self.BaseStationController.UpdateBionodeRegisters(version, amplitude, pulseRepeatTime, pulseWidth, numStimCycles, totalSampleRate, ADCResolution, StartStim, StopStim, StartImpedanceTest1, StopImpedanceTest1, StartImpedanceTest2, StopImpedanceTest2, StartThermalTest, self.ADCChannelRatio, positiveCalibration, negativeCalibration, cathodicCalibration, anodicCalibration, zeroCalibration, ADC1InputCode, ADC2InputCode, ADC3InputCode, ADC4InputCode, PressureReadingConfig, PotCH1V1PressureData, PotCH1V2PressureData, PotCH2V1PressureData, PotCH2V2PressureData)

        self.pw1.setTitle(title=self.ui.ADC1Input_cb.currentText())
        self.pw2.setTitle(title=self.ui.ADC2Input_cb.currentText())
        self.pw3.setTitle(title=self.ui.ADC3Input_cb.currentText())
        self.pw4.setTitle(title=self.ui.ADC4Input_cb.currentText())

        if (self.ui.Enable4Channels_cb.isChecked()):#self.ui.Enable4Channels_cb.isChecked()):
            self.numberOfChannelsDisplayed = 4
            self.SampleRate = totalSampleRate/4
            self.pw1.setXRange(0, self.WindowSize/self.SampleRate/2)
            self.pw2.setXRange(0, self.WindowSize/self.SampleRate/2)
            self.pw3.setXRange(0, self.WindowSize/self.SampleRate/2)
            self.pw4.setXRange(0, self.WindowSize/self.SampleRate/2)

            ch1gain = int(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch1gain'])
            ch2gain = int(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch2gain'])
            ch3gain = int(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch3gain'])
            ch4gain = int(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch4gain'])
            self.pw1.setYRange(-self.VoltsPerBit*128/ch1gain, self.VoltsPerBit*128/ch1gain)
            self.pw2.setYRange(-self.VoltsPerBit*128/ch2gain, self.VoltsPerBit*128/ch2gain)
            self.pw3.setYRange(-self.VoltsPerBit*128/ch3gain, self.VoltsPerBit*128/ch3gain)
            self.pw4.setYRange(-self.VoltsPerBit*128/ch4gain, self.VoltsPerBit*128/ch4gain)

            self.updatePlotVisibility(4)

        else:
            self.numberOfChannelsDisplayed = 2
            self.SampleRate = totalSampleRate/2
            self.pw1.setXRange(0, self.WindowSize/self.SampleRate)
            self.pw2.setXRange(0, self.WindowSize/self.SampleRate)

            ch1gain = int(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch1gain'])
            ch2gain = int(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch2gain'])
            self.pw1.setYRange(-self.VoltsPerBit*128/ch1gain, self.VoltsPerBit*128/ch1gain)
            self.pw2.setYRange(-self.VoltsPerBit*128/ch2gain, self.VoltsPerBit*128/ch2gain)

            self.updatePlotVisibility(2)


    def createNewRecordingFile(self):
        #First, note the current time and clear the base-station time
        self.recordingFileSize = 0
        self.savedInitiatedTimeStamp = self.prevTimeStamp
        timeStamp = datetime.datetime.now()
        fileName = str(timeStamp.year) + '.' + str(timeStamp.month) +  '.' + str(timeStamp.day) +  '.' + str(timeStamp.hour) +  '.' + str(timeStamp.minute) +  '.' + str(timeStamp.second) + '_' + self.ui.Bionode_cb.currentText()

        #Update the base station registers to indicate that the timestamp should be reset.
        self.UpdateBaseStationRegistersFromUI(True)

        totalSampleRate = self.getSampleRateFromComboBox()

        #Now, make a file and fill in the timestamp information as the top packet
        self.recordingFile = open (fileName + '.bin', "wb")
        tsByteArray = bytearray([timeStamp.year>>8, timeStamp.year & 0xFF, timeStamp.month, timeStamp.day, timeStamp.hour, timeStamp.minute, timeStamp.second,(totalSampleRate>>8)&0xFF, totalSampleRate&0xFF, self.numberOfChannelsDisplayed&0xFF]) 
        self.recordingFile.write(tsByteArray)
        self.recordingFile.write(bytearray([0 for i in range(BaseStation.BytesPerBSPacket - 10)]))

    def myRecordButtonClicked(self):
        if (self.recording == False):
            self.createNewRecordingFile()
            self.recording = True
            self.ui.Record_b.setText('Stop')

        else:
            self.recording = False
            self.ui.Record_b.setText('Record')

    def data_listener(self):
        ready = select.select([self.BaseStationController.socket_TXRX], [], [])
        newData = []
        addr = -1

        self.stoppedSocket = False

        while not self.closing:
            ready = select.select([self.BaseStationController.socket_TXRX], [], [])
            if (ready[0]):
                newData, addr = self.BaseStationController.socket_TXRX.recvfrom(1024) # buffer size is 1024 bytes
                if (addr[0] != self.UDP_TX_IP):
                    newData = []
                if len(newData) > 0:
                    i = 0
                    validPackets = []
                    for p in range(int(len(newData)/BytesPerBSPacket)):
                        rawData = newData[i:i+BytesPerBSPacket]
                        i += BytesPerBSPacket
                        packet = BaseStation.GetPacket(rawData, CRCPoly)       
                        if (packet.valid):
                            validPackets.append(packet)
                        else:
                            print('Invalid Data Packet!')

                    #Route all valid packets to their correct positions
                    for p in validPackets:
                        #If recording, save all valid packets
                        if self.recording:
                            if self.recordingFileSize > 1000000000:
                                self.recordingFile.close()
                                self.createNewRecordingFile()

                            self.recordingFile.write(bytearray(p.rawData))
                            self.recordingFileSize = self.recordingFileSize + len(p.rawData)

                            if (not self.recording):
                                self.recordingFile.close()

                        #Update display buffer to display all data packets
                        if p.PacketType == BaseStation.DataPacketType.Data_8bit:
                            self.VoltsPerBit = 1.2/256
                            self.dataToDisplay.extend(p.DataPoints)
                            
                        elif p.PacketType == BaseStation.DataPacketType.Data_10bit:
                            #Update VoltsPerBit
                            self.VoltsPerBit = 1.2/1024
                            dp = BaseStation.DataPacket(p.rawData, p.CRCPoly, 10)
                            self.dataToDisplay.extend(dp.DataPoints)

                        elif p.PacketType == BaseStation.DataPacketType.Impedance:
                            self.VoltsPerBit = 1.2/256
                            self.dataToDisplay.extend(p.DataPoints)

                        elif p.PacketType == BaseStation.DataPacketType.Thermal:
                            self.VoltsPerBit = 1.2/256
                            self.dataToDisplay.extend(p.DataPoints)
                            self.newThermalDataReady = True
                            self.thermalData = (p.ThermalData[0]*256 + p.ThermalData[1])*0.0625

        self.stoppedSocket = True

    def updatePlot(self):
        global displayData, dataToDisplay, newThermalDataReady

        if (self.newThermalDataReady):
            self.ui.ImplantTemperature_te.setText(str(self.thermalData))
            self.newThermalDataReady = False
            #Send packet to Basestation to stop future thermal updates
            self.UpdateBionodeRegistersFromUI(False,False, False, False, False)


        newData = list(self.dataToDisplay)
        self.dataToDisplay = []
        newDisplay = list(self.displayData[len(newData):len(self.displayData)] + newData)
        self.displayData = list(newDisplay)
        if len(self.displayData) > (self.WindowSize*2):
            print('overflow!')
            self.displayData = self.displayData[-(self.WindowSize*2):]

        if not self.displayPaused:

            ch1gain = int(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch1gain'])
            ch2gain = int(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch2gain'])

            if (self.ADCChannelRatio == '1:1'):
                if (self.numberOfChannelsDisplayed == 4):#self.ui.Enable4Channels_cb.isChecked()):
                    ch3gain = int(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch3gain'])
                    ch4gain = int(self.ConfigSectionMap(self.bionodeConfig,self.ui.Bionode_cb.currentText())['ch4gain'])

                    self.p1.setData(y=[(i*self.VoltsPerBit-0.6)/ch1gain for i in self.displayData[::4]], x=[i/self.SampleRate for i in range(int(self.WindowSize/2))])
                    self.p2.setData(y=[(i*self.VoltsPerBit-0.6)/ch2gain for i in self.displayData[1::4]], x=[i/self.SampleRate for i in range(int(self.WindowSize/2))])       
                    self.p3.setData(y=[(i*self.VoltsPerBit-0.6)/ch3gain for i in self.displayData[2::4]], x=[i/self.SampleRate for i in range(int(self.WindowSize/2))])
                    self.p4.setData(y=[(i*self.VoltsPerBit-0.6)/ch4gain for i in self.displayData[3::4]], x=[i/self.SampleRate for i in range(int(self.WindowSize/2))])
                else:
                    self.p1.setData(y=[(i*self.VoltsPerBit-0.6)/ch1gain for i in self.displayData[::2]], x=[i/self.SampleRate for i in range(int(self.WindowSize))])
                    self.p2.setData(y=[(i*self.VoltsPerBit-0.6)/ch2gain for i in self.displayData[1::2]], x=[i/self.SampleRate for i in range(int(self.WindowSize))])
            
            elif (self.ADCChannelRatio == '1:3'):
                self.p1.setData(y=[(i*self.VoltsPerBit-0.6)/ch1gain for i in self.displayData[::4]], x=[i/self.SampleRate*2 for i in range(len(self.displayData[::4]))])

                ch2DisplayList = list(self.displayData)
                del(ch2DisplayList[::4])

                self.p2.setData(y=[(i*self.VoltsPerBit-0.6)/ch2gain for i in ch2DisplayList], x=[i/self.SampleRate*2/3 for i in range(len(ch2DisplayList))])

            elif (self.ADCChannelRatio == '1:39'):
                self.p1.setData(y=[(i*self.VoltsPerBit-0.6)/ch1gain for i in self.displayData[::40]], x=[i/self.SampleRate*20 for i in range(len(self.displayData[::40]))])

                ch2DisplayList = list(self.displayData)
                del(ch2DisplayList[::40])

                self.p2.setData(y=[(i*self.VoltsPerBit-0.6)/ch2gain for i in ch2DisplayList], x=[i/self.SampleRate*20/39 for i in range(len(ch2DisplayList))])

if __name__ == '__main__':
    import sys
    app = QtGui.QApplication(sys.argv)
    mySW = ControlMainWindow()
    mySW.show()
    sys.exit(app.exec_())
