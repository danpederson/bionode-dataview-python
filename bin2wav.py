import struct
import math
import BaseStation
import wave
import array
import os

import sys
if (len(sys.argv) != 2):
	print('bin2wav <Binary_File_Name>')

else:
	fileName = sys.argv[1]
	validCode = pow(2,10)-1

	#resolution = 8
	endOfFile = False
	BytesPerPacket = 58
	#PointsPerPacket = 40
	CRCPoly = BaseStation.CRCPoly

	print('Opening ', fileName)
	statInfo = os.stat(fileName)
	print(int(statInfo.st_size))
	print(fileName, 'Contains ', str(int(statInfo.st_size)/BytesPerPacket), ' packets')

	totalPackets = int(int(statInfo.st_size)/BytesPerPacket)

	#f = open("2015.11.18.11.35.14.bin", "rb")
	f = open(fileName, "rb")

	packetList = []

	def GetNextPacket():
		global f, byte, endOfFile
		plist = []
		while (len(byte) != 0) and (len(plist) < 58):
			try:
				b = struct.unpack('B',byte)[0]
			except:
				print(byte)
				b = 0
			plist.append(b)
			byte = f.read(1)

		if len(byte) == 0:
			endOfFile = True
			print('end!')
			return BaseStation.GetPacket([0 for i in range(BytesPerPacket)],CRCPoly)
		
		else:
			return BaseStation.GetPacket(plist,CRCPoly)

	try:
		#Skip over the header packet
		print('Stripping off header packet')
		byte = f.read(1)
		GetNextPacket()

		#Read in the first 100 packets
		i = 0
		resolution = -1
		while (i < 100) and (not endOfFile):
			p = GetNextPacket()
			if p.PacketType == BaseStation.DataPacketType.Data_8bit:
				packetList.append(p)
				i = i+1
				if (resolution == -1):
					print('8-bit Resolution detected')
					resolution = 8
					PointsPerPacket = 40
					targetPacketType = BaseStation.DataPacketType.Data_8bit
				else:
					print('Mixed data resolution!')
			elif p.PacketType == BaseStation.DataPacketType.Data_10bit:
				packetList.append(p)
				i = i+1
				if (resolution == -1):
					print('10-bit Resolution detected')
					resolution = 10
					PointsPerPacket = 32
					targetPacketType = BaseStation.DataPacketType.Data_10bit
				else:
					print('Mixed data resolution!')

		#Look for, and remove any leading OLD packets
		for i in range(1,len(packetList)):
			if (packetList[i].Timestamp < packetList[i-1].Timestamp):
				print('Timestamp reset found. Removing ',i,' leading packet(s)')
				packetList = packetList[i:]
				break

		#determine the sample rate of the data

		subsequentPacketTimeDiffs = []
		for i in range(1,len(packetList)):
			if packetList[i].PID == packetList[i-1].PID+1:
				if(packetList[i].Timestamp > packetList[i-1].Timestamp):
					#print('TimeDiff = ',str(packetList[i].Timestamp - packetList[i-1].Timestamp))
					subsequentPacketTimeDiffs.append(packetList[i].Timestamp - packetList[i-1].Timestamp)

		expectedTimeDiff = sum(subsequentPacketTimeDiffs)/len(subsequentPacketTimeDiffs)
		print('Expected Time Diff = ', str(expectedTimeDiff))
		sampleRate = 1/(expectedTimeDiff/PointsPerPacket*32e-6)
		print('Total Sample rate = ', sampleRate, ' Hz')

		#Fill these initially with all data in plist!
		dataFile_ch1 = []
		dataFile_ch2 = []
		validFile = []

		prevPacket = packetList[0]

		dataFile_ch1.extend(prevPacket.DataPoints[::2])
		dataFile_ch2.extend(prevPacket.DataPoints[1::2])
		validFile.extend([1 for i in range(PointsPerPacket>>1)])

		for i in range(1,len(packetList)):
			nextPacket = packetList[i]

			#if (nextPacket.PacketType == BaseStation.DataPacketType.Data_8bit):
			if (nextPacket.PacketType == targetPacketType):
				
				missingDataPoints = int((nextPacket.Timestamp - prevPacket.Timestamp-expectedTimeDiff)/(expectedTimeDiff/PointsPerPacket))
				
				if missingDataPoints != 0:
					missingDataPoints = missingDataPoints + 1
					dataFile_ch1.extend([int(math.pow(2,resolution-1)) for i in range(missingDataPoints>>1)])
					dataFile_ch2.extend([int(math.pow(2,resolution-1)) for i in range(missingDataPoints>>1)])
					validFile.extend([0 for i in range(missingDataPoints>>1)])
					print (missingDataPoints)

				dataFile_ch1.extend(nextPacket.DataPoints[::2])
				dataFile_ch2.extend(nextPacket.DataPoints[1::2])
				validFile.extend([validCode for i in range(PointsPerPacket>>1)])
				prevPacket = nextPacket

	#	packetFile.append(prevPacket)
		packetsParsed = 100
		while (not endOfFile):
			nextPacket = GetNextPacket()
			packetsParsed = packetsParsed + 1
			if (packetsParsed%1000 == 0):
				print('Packets Parsed: ',packetsParsed, '/',totalPackets)
			if (endOfFile):
				break
			#if (nextPacket.PacketType == BaseStation.DataPacketType.Data_8bit):
			if (nextPacket.PacketType == targetPacketType):
				
				missingDataPoints = int((nextPacket.Timestamp - prevPacket.Timestamp-expectedTimeDiff)/(expectedTimeDiff/PointsPerPacket))
				
				if missingDataPoints != 0:
					missingDataPoints = missingDataPoints + 1
					dataFile_ch1.extend([int(math.pow(2,resolution-1)) for i in range(missingDataPoints>>1)])
					dataFile_ch2.extend([int(math.pow(2,resolution-1)) for i in range(missingDataPoints>>1)])
					validFile.extend([0 for i in range(missingDataPoints>>1)])
					print ('Detected ',str(missingDataPoints), 'missing data points')

				dataFile_ch1.extend(nextPacket.DataPoints[::2])
				dataFile_ch2.extend(nextPacket.DataPoints[1::2])
				validFile.extend([validCode for i in range(PointsPerPacket>>1)])
				prevPacket = nextPacket

		print(sum(validFile)/len(validFile)/validCode)

		print('Generating Wave File...')
		outFileName = os.path.splitext(fileName)[0] + '.wav'
		wavOut = wave.open(outFileName,'w')
		wavOut.setparams((3,2,int(sampleRate/2),0,'NONE','not compressed'))

		wavOut.writeframes(array.array('h',[(val<<(10-resolution)) for tripplet in zip(dataFile_ch1, dataFile_ch2, validFile) for val in tripplet]))

	finally:
		f.close()